package com.invest.crypto.common.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class provides utility methods for calculating HMAC-X based signature code and cryptographically strong identification code.
 * 
 * @author Kiril.m
 */
public class SignatureUtil {

	private static final String HMAC_ALGORITHM = "HmacSHA512";
	private static final int NONCE_BYTE_LENGTH = 32;
	private static final SecureRandom random = new SecureRandom();
	private static Key secretKeySpec;
	static {
		byte[] passBytes = SignatureManager.getApiSecret();
		secretKeySpec = new SecretKeySpec(passBytes, HMAC_ALGORITHM);
		Arrays.fill(passBytes, (byte) 0); // clear sensitive data
	}

	/**
	 * Calculates signature for signing based on the standard HMAC-SHA512 algorithm. For more information see
	 * {@link SignatureUtil#HMAC_ALGORITHM}.
	 * 
	 * @param data The message used for authentication code
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws IllegalStateException
	 * @throws UnsupportedEncodingException
	 * 
	 * @see SignatureUtil#HMAC_ALGORITHM
	 */
	public static String calculateSignature(String data)	throws NoSuchAlgorithmException, InvalidKeyException, IllegalStateException,
															UnsupportedEncodingException {
		Mac mac = Mac.getInstance(HMAC_ALGORITHM);
		mac.init(secretKeySpec);
		return HexUtil.byteArrayToHexString(mac.doFinal(data.getBytes("UTF-8")));
	}

	/**
	 * Creates a {@code SecureRandom} array of bytes with length specified by {@link SignatureUtil#NONCE_BYTE_LENGTH}, by default 32. The
	 * method returns the identification code as {@code String} with respect of a <a
	 * href=https://en.wikipedia.org/wiki/Cryptographic_nonce>cryptographic nonce</a>.
	 * 
	 * @return The sequence of bytes as {@code String}
	 * 
	 * @see SignatureUtil#random
	 * @see SignatureUtil#NONCE_BYTE_LENGTH
	 * @see <a href=https://en.wikipedia.org/wiki/Cryptographic_nonce>Cryptographic_nonce</a>
	 */
	public static String createNonce() {
		byte[] bytes = new byte[NONCE_BYTE_LENGTH];
		random.nextBytes(bytes);
		return Base64.getEncoder().encodeToString(bytes);
	}
}