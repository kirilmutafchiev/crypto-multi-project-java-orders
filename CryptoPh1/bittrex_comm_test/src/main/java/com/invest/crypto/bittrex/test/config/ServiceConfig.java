/**
 * 
 */
package com.invest.crypto.bittrex.test.config;

import java.io.Serializable;

/**
 * @author Tabakov
 *
 */
public class ServiceConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5691731329258501836L;

	private int id;
	private String serviceName;
	private boolean isSuccess;
	private String message;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ServiceConfig [id=" + id + ", serviceName=" + serviceName + ", isSuccess=" + isSuccess + ", message="
				+ message + "]";
	}

}
