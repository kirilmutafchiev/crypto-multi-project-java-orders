package com.invest.crypto.bittrex.test.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.invest.crypto.common.util.DAOBase;

public class ConfigDAO extends DAOBase {

	public static Map<String, String> getServiceConfig(Connection con, String serviceName) throws SQLException {
		Map<String, String> serviceConfig = new HashMap<>();

		String sql = "SELECT cr.test_key, cr.test_value FROM crypto_config_test_api cr WHERE cr.service_name = ?";

		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, serviceName);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				serviceConfig.put(rs.getString("test_key"), rs.getString("test_value"));
			}
		}
		return serviceConfig;
	}

}
