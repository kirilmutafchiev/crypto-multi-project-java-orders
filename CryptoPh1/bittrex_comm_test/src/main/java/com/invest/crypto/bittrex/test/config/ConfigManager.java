/**
 * 
 */
package com.invest.crypto.bittrex.test.config;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.common.util.ManagerBase;

/**
 * @author Tabakov
 *
 */
public class ConfigManager extends ManagerBase {

	private static final Logger log = LogManager.getLogger(ConfigManager.class);

	public static Map<String, String> getConfig(String serviceName) {
		Connection con = null;
		try {
			con = getConnection();
			return ConfigDAO.getServiceConfig(con, serviceName);
		} catch (SQLException e) {
			log.debug("Unable to get configuration for test service" + serviceName + "due to: ", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

}
