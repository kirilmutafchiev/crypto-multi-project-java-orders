/**
 * 
 */
package com.invest.crypto.bittrex.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invest.crypto.bittrex.config.EnvironmentConfigManager;
import com.invest.crypto.bittrex.model.accountapi.Balance;
import com.invest.crypto.bittrex.model.accountapi.Order;
import com.invest.crypto.bittrex.model.accountapi.OrderHistoryEntry;
import com.invest.crypto.bittrex.model.accountapi.WithdrawalHistoryEntry;
import com.invest.crypto.bittrex.model.accountapi.WithdrawalRequested;
import com.invest.crypto.bittrex.util.Util;
import com.invest.crypto.common.security.SignatureUtil;

/**
 * @author Tabakov
 *
 */
@Path("/accountapi")
public class BittrexAccountAPI {
	
	private static final Logger log = LogManager.getLogger(BittrexAccountAPI.class);

//	@GET
//	@Path("/getorder")//fully tested
//	@Produces(MediaType.APPLICATION_JSON)
	public Order getOrder(/*@QueryParam("uuid")*/ String uuid) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("getorder is called with params --> uuid: " + uuid);
		
		String nonce = SignatureUtil.createNonce();
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/account/getorder?uuid=" + uuid + "&apikey=" + Util.API_KEY + "&nonce=" + nonce;
		
		String requestURL = "getOrderHistory URL: " + baseUrl + "/account/getorder?uuid=" + uuid + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		Order order = new Order();
		order.setResponse(jsonResponse);
		order.setRequestURL(requestURL);
		
		
		log.debug("Bittrex response for getorder: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();

		JsonNode orderResponse = objectMapper.readTree(jsonResponse);
		String success = orderResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = orderResponse.get("result");
			if(!result.isNull()) {
				order.setAccountId(result.get("AccountId").isNull() ? null : result.get("AccountId").asText());
				order.setOrderUuid(result.get("OrderUuid").isNull() ? null : result.get("OrderUuid").asText());
				order.setExchange(result.get("Exchange").isNull() ? null : result.get("Exchange").asText());
				order.setType(result.get("Type").isNull() ? null : result.get("Type").asText());
				order.setQuantity(result.get("Quantity").isNull() ? null : result.get("Quantity").decimalValue());
				order.setQuantityRemaining(result.get("QuantityRemaining").isNull() ? null : result.get("QuantityRemaining").decimalValue());
				order.setLimit(result.get("Limit").isNull() ? null : result.get("Limit").decimalValue());
				order.setReserved(result.get("Reserved").isNull() ? null : result.get("Reserved").decimalValue());
				order.setReserveRemaining(result.get("ReserveRemaining").isNull() ? null : result.get("ReserveRemaining").decimalValue());
				order.setCommissionReserved(result.get("CommissionReserved").isNull() ? null : result.get("CommissionReserved").decimalValue());
				order.setCommissionReserveRemaining(result.get("CommissionReserveRemaining").isNull() ? null : result.get("CommissionReserveRemaining").decimalValue());
				order.setCommissionPaid(result.get("CommissionPaid").isNull() ? null : result.get("CommissionPaid").decimalValue());
				order.setPrice(result.get("Price").isNull() ? null : result.get("Price").decimalValue());
				order.setPricePerUnit(result.get("PricePerUnit").isNull() ? null : result.get("PricePerUnit").decimalValue());
				order.setOpened(result.get("Opened").isNull() ? null : LocalDateTime.parse(result.get("Opened").asText()));
				order.setClosed(result.get("Closed").isNull() ? null : LocalDateTime.parse(result.get("Closed").asText()));
				order.setIsOpen(result.get("IsOpen").isNull() ? null : result.get("IsOpen").asBoolean());
				order.setSentinel(result.get("Sentinel").isNull() ? null : result.get("Sentinel").asText());
				order.setCancelInitiated(result.get("CancelInitiated").isNull() ? null : result.get("CancelInitiated").asBoolean());
				order.setImmediateOrCancel(result.get("ImmediateOrCancel").isNull() ? null : result.get("ImmediateOrCancel").asBoolean());
				order.setIsConditional(result.get("IsConditional").isNull() ? null : result.get("IsConditional").asBoolean());
				order.setCondition(result.get("Condition").isNull() ? null : result.get("Condition").asText());
				order.setConditionTarget(result.get("ConditionTarget").isNull() ? null : result.get("ConditionTarget").asText());
			}
		}
		return order;
	}
	
//	@GET
//	@Path("/getorderhistory")// fully tested
//	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<OrderHistoryEntry> getOrderHistory(String market) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("getorderhistory is called with params --> market: " + market);
		
		String nonce = SignatureUtil.createNonce();
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/account/getorderhistory?market=" + market + "&apikey=" + Util.API_KEY + "&nonce=" + nonce;
		
		String requestURL = "getOrderHistory URL: " + baseUrl + "/account/getorderhistory?market=" + market + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		OrderHistoryEntry orderHistoryEntryResponse = new OrderHistoryEntry();
		
		orderHistoryEntryResponse.setResponse(jsonResponse);
		orderHistoryEntryResponse.setRequestURL(requestURL);
		
		
		log.debug("Bittrex response for getorderhistory: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayList<OrderHistoryEntry> orderHistoryList = new ArrayList<OrderHistoryEntry>();
		// add whole Bittrex response to list
		orderHistoryList.add(orderHistoryEntryResponse);

		JsonNode orderResponse = objectMapper.readTree(jsonResponse);
		String success = orderResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = orderResponse.get("result");
			if(!result.isNull()) {
				if (result.isArray()) {
					for (JsonNode o : result) {
						OrderHistoryEntry orderHistoryEntry = new OrderHistoryEntry();
						orderHistoryEntry.setOrderUuid(o.get("OrderUuid").isNull() ? null : o.get("OrderUuid").asText());
						orderHistoryEntry.setExchange(o.get("Exchange").isNull() ? null : o.get("Exchange").asText());
						orderHistoryEntry.setTimeStamp(o.get("TimeStamp").isNull() ? null : LocalDateTime.parse(o.get("TimeStamp").asText()));
						orderHistoryEntry.setOrderType(o.get("OrderType").isNull() ? null : o.get("OrderType").asText());
						orderHistoryEntry.setLimit(o.get("Limit").isNull() ? null : o.get("Limit").decimalValue());
						orderHistoryEntry.setQuantity(o.get("Quantity").isNull() ? null : o.get("Quantity").decimalValue());
						orderHistoryEntry.setQuantityRemaining(o.get("QuantityRemaining").isNull() ? null : o.get("QuantityRemaining").decimalValue());
						orderHistoryEntry.setCommission(o.get("Commission").isNull() ? null : o.get("Commission").decimalValue());
						orderHistoryEntry.setPrice(o.get("Price").isNull() ? null : o.get("Price").decimalValue());
						orderHistoryEntry.setPricePerUnit(o.get("PricePerUnit").isNull() ? null : o.get("PricePerUnit").decimalValue());
						orderHistoryEntry.setIsConditional(o.get("IsConditional").isNull() ? null : o.get("IsConditional").asBoolean());
						orderHistoryEntry.setCondition(o.get("Condition").isNull() ? null : o.get("Condition").asText());
						orderHistoryEntry.setConditionTarget(o.get("ConditionTarget").isNull() ? null :o.get("ConditionTarget").asText());
						orderHistoryEntry.setImmediateOrCancel(o.get("ImmediateOrCancel").isNull() ? null : o.get("ImmediateOrCancel").asBoolean());
						orderHistoryList.add(orderHistoryEntry);
					}
				}
		  }

		}
		return orderHistoryList;
	}
	
	
//	@GET
//	@Path("/withdraw")// fully tested
//	@Produces(MediaType.APPLICATION_JSON)
	public WithdrawalRequested withdraw(/*@QueryParam("currency")*/ String currency, /*@QueryParam("quantity")*/ BigDecimal quantity, /*@QueryParam("address")*/ String address) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("withdraw is called with params --> currency: " + currency + "quantity: " + quantity + "address: " + address);
		
		String nonce = SignatureUtil.createNonce();
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/account/withdraw?currency=" + currency + "&apikey=" + Util.API_KEY + "&quantity=" + quantity + "&address=" + address + "&nonce=" + nonce;
		
		String requestURL = "withdraw URL: " + baseUrl + "/account/withdraw?currency=" + currency + "&quantity=" + quantity + "&address=" + address + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		WithdrawalRequested withdrawalRequested = new WithdrawalRequested();
		
		withdrawalRequested.setResponse(jsonResponse);
		withdrawalRequested.setRequestURL(requestURL);
		
		log.debug("Bittrex response for withdraw: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();

		JsonNode marketsResponse = objectMapper.readTree(jsonResponse);
		String success = marketsResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = marketsResponse.get("result");
			if(!result.isNull()) {
				withdrawalRequested.setUuid(result.get("uuid").isNull() ? null : result.get("uuid").asText());
			}
		}
		
		return withdrawalRequested;
	}
	
//	@GET
//	@Path("/getwithdrawalhistory")
//	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<WithdrawalHistoryEntry> getWithdrawalHistory(@QueryParam("currency") String currency) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("getwithdrawalhistory is called with params --> currency: " + currency);
		
		String nonce = SignatureUtil.createNonce();
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/account/getwithdrawalhistory?currency=" + currency + "&apikey=" + Util.API_KEY + "&nonce=" + nonce;
		
		String requestURL = "getWithdrawalHistory URL: " + baseUrl + "/account/getwithdrawalhistory?currency=" + currency + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		WithdrawalHistoryEntry withdrawalHistoryEntry = new WithdrawalHistoryEntry();
		
		withdrawalHistoryEntry.setResponse(jsonResponse);
		withdrawalHistoryEntry.setRequestURL(requestURL);
		
		
		log.debug("Bittrex response for getwithdrawalhistory: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayList<WithdrawalHistoryEntry> withdrawalHistoryList = new ArrayList<WithdrawalHistoryEntry>();
		// add whole Bittrex response to list
		withdrawalHistoryList.add(withdrawalHistoryEntry);

		JsonNode orderResponse = objectMapper.readTree(jsonResponse);
		String success = orderResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = orderResponse.get("result");
			if(!result.isNull()) {
				if (result.isArray()) {
					for (JsonNode o : result) {
						WithdrawalHistoryEntry withdrawalHistEntry = new WithdrawalHistoryEntry();
						withdrawalHistEntry.setPaymentUuid(o.get("PaymentUuid").isNull() ? null : o.get("PaymentUuid").asText());
						withdrawalHistEntry.setCurrency(o.get("Currency").isNull() ? null : o.get("Currency").asText());
						withdrawalHistEntry.setAmount(o.get("Amount").isNull() ? null : o.get("Amount").asText());
						withdrawalHistEntry.setAddress(o.get("Address").isNull() ? null : o.get("Address").asText());
						withdrawalHistEntry.setOpened(o.get("Opened").isNull() ? null : LocalDateTime.parse(o.get("Opened").asText()));
						withdrawalHistEntry.setAuthorized(o.get("Authorized").isNull() ? null : o.get("Authorized").asBoolean());
						withdrawalHistEntry.setPendingPayment(o.get("PendingPayment").isNull() ? null : o.get("PendingPayment").asBoolean());
						withdrawalHistEntry.setTxCost(o.get("TxCost").isNull() ? null : o.get("TxCost").asDouble());
						withdrawalHistEntry.setTxId(o.get("TxId").isNull() ? null : o.get("TxId").asText());
						withdrawalHistEntry.setCanceled(o.get("Canceled").isNull() ? null :o.get("Canceled").asBoolean());
						withdrawalHistEntry.setInvalidAddress(o.get("InvalidAddress").isNull() ? null :o.get("InvalidAddress").asBoolean());
						withdrawalHistoryList.add(withdrawalHistEntry);
					}
				}
		  }

		}
		return withdrawalHistoryList;
	}
	
//	@GET
//	@Path("/getbalance")
//	@Produces(MediaType.APPLICATION_JSON)
	public Balance getBalance(@QueryParam("currency") String currency) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("getbalance is called with params --> currency: " + currency);
		
		String nonce = SignatureUtil.createNonce();
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/account/getbalance?currency=" + currency + "&apikey=" + Util.API_KEY + "&nonce=" + nonce;
		
		String requestURL = "getBalance URL: " + baseUrl + "/account/getbalance?currency=" + currency + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		Balance balance = new Balance();
		balance.setResponse(jsonResponse);
		balance.setRequestURL(requestURL);
		
		
		log.debug("Bittrex response for getBalance: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();

		JsonNode orderResponse = objectMapper.readTree(jsonResponse);
		String success = orderResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = orderResponse.get("result");
			if(!result.isNull()) {
				balance.setCurrency(result.get("Currency").isNull() ? null : result.get("Currency").asText());
				balance.setBalance(result.get("Balance").isNull() ? null : result.get("Balance").decimalValue());
				balance.setAvailable(result.get("Available").isNull() ? null : result.get("Available").decimalValue());
				balance.setPending(result.get("Pending").isNull() ? null : result.get("Pending").decimalValue());
				balance.setCryptoAddress(result.get("CryptoAddress").isNull() ? null : result.get("CryptoAddress").asText());
				// balance.setRequested(result.get("Requested").isNull() ? null : result.get("Requested").asBoolean());
				// balance.setUuid(result.get("Uuid").isNull() ? null : result.get("Uuid").asText());
			}
		}
		return balance;
	}
}
