/**
 * 
 */
package com.invest.crypto.bittrex.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.invest.crypto.common.util.DAOBase;

/**
 * @author Tabakov
 *
 */
public class EnvironmentConfigDAO extends DAOBase {
	
	public static Map<String, String> getBaseUrl(Connection con) throws SQLException {
		String sql = "SELECT a.config_value FROM crypto_config a WHERE a.config_key =\'URL\'";
		Map<String, String> urlConfig = new HashMap<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				urlConfig.put("URL", rs.getString("config_value"));
			}
		}
		return urlConfig;
	}
	
	public static long getTickerHashInterval(Connection con) throws SQLException {
		String sql = "SELECT a.config_value FROM crypto_config a WHERE a.config_key =\'TICKER_HASH_INTERVAL\'";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("config_value");
			}
		}
		return EnvironmentConfigManager.TIME_TO_WAIT_30_SEC;
	}
	
	public static long getTickerHashIntervalCoinMarketCap(Connection con) throws SQLException {
		String sql = "SELECT a.config_value FROM crypto_config a WHERE a.config_key =\'TICKER_HASH_INTERVAL_COINMARKETCAP\'";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("config_value");
			}
		}
		return EnvironmentConfigManager.TIME_TO_WAIT_30_SEC;
	}
	
	public static Map<String, String> getAvailableCurrencies(Connection con) throws SQLException {
		String sql = "SELECT code, display_name FROM coinmarketcap_fiat_currencies";
		Map<String, String> currencies = new HashMap<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				currencies.put(rs.getString("code"), rs.getString("display_name"));
			}
		}
		return currencies;
	}

}
