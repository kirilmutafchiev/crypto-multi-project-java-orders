package com.invest.crypto.services.order.check;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.invest.crypto.services.order.OrderDAO;
import com.invest.crypto.services.order.OrderManager.OrderState;

/**
 * @author Kiril.m
 */
class CheckOrderDAO extends OrderDAO {

	static List<CheckOrder> getOrders(Connection con/* , int stateId */) throws SQLException {
		String sql = "select id, uuid, to_currency, quantity, wallet, transaction_id, status, bittrex_commission from crypto_trade "
					+ "where status in (" + OrderState.PROCESSED.getId() + "," + OrderState.PROCESSING_SKIPPED.getId() + ")";
		List<CheckOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			// ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CheckOrder order = new CheckOrder(	rs.getLong("id"), rs.getString("uuid"), rs.getString("to_currency"),
													rs.getBigDecimal("quantity"), rs.getString("wallet"), rs.getLong("transaction_id"),
													OrderState.getById(rs.getInt("status")), rs.getBigDecimal("bittrex_commission"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean closeOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_trade set status = " + OrderState.CLOSED.getId() + ", time_done = sysdate where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static void insertWithdraw(Connection con, CheckOrder checkOrder, OrderState state) throws SQLException {
		String sql = "insert into crypto_withdraw (id, currency, quantity, wallet, transaction_id, crypt_trade_id, status, bittrex_commission, time_created) "
					+ "values (?, ?, ?, ?, ?, ?, ?, ?, sysdate)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			int i = 1;
			long id = getSequenceNextVal(con, "SEQ_CRYPTO_WITHDRAW");
			ps.setLong(i++, id);
			ps.setString(i++, checkOrder.getToCurrency());
			ps.setBigDecimal(i++, checkOrder.getQuantity());
			ps.setString(i++, checkOrder.getWallet());
			ps.setLong(i++, checkOrder.getTransactionId());
			ps.setLong(i++, checkOrder.getId());
			ps.setInt(i++, state.getId());
			ps.setBigDecimal(i++, checkOrder.getCommission());
			ps.executeUpdate(); // TODO result?
		}
	}

	static boolean isManualWithdrawMode(Connection con) throws SQLException {
		String sql = "select config_value from crypto_config where config_key = '" + CheckOrderManager.WITHDRAW_MANUAL_MODE + "'";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getBoolean("config_value");
			}
		}
		return false;
	}
}