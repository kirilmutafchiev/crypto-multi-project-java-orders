package com.invest.crypto.services.order.buy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.services.order.OrderConfig;
import com.invest.crypto.services.order.OrderService;

/**
 * @author Kiril.m
 */
public class BuyOrderService extends OrderService /*implements BuyOrderServiceMXBean*/ {

	private static final Logger log = LogManager.getLogger(BuyOrderService.class);
	private String apiKey;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.invest.crypto.services.order.OrderService#initService()
	 */
	@Override
	public void initService() {
		OrderConfig config = BuyOrderManager.getOrderConfig();
		if (config != null) {
			apiKey = config.getApiKey();
			executor = new BuyOrderExecutor(config.getTaskCount());
			scheduledPeriod = config.getScheduledPeriod();
		} else {
			log.warn("Executor is null. Scheduling of task will fail");
		}
	}

	@Override
	protected Logger getLogger() {
		return log;
	}

	private class BuyOrderExecutor implements Runnable {

		private ExecutorService taskExecutor;

		public BuyOrderExecutor(int taskCount) {
			taskExecutor = Executors.newFixedThreadPool(taskCount);
		}

		@Override
		public void run() {
			log.debug("BuyOrderExecutor has started");
			List<BuyOrder> buyOrders = BuyOrderManager.getOrders(/* 1 */);
			List<Future<?>> futures = new ArrayList<>();
			for (BuyOrder bo : buyOrders) {
				futures.add(taskExecutor.submit(new BuyOrderTask(apiKey, bo)));
			}
			for (Future<?> future : futures) {
				try {
					future.get(); // in order to wait all tasks
				} catch (InterruptedException e) {
					log.debug("Thread was interrupted", e);
					// Preserve interrupt status
					Thread.currentThread().interrupt();
				} catch (ExecutionException e) {
					log.debug("Unable to retrieve result", e);
				}
			}
			log.debug("BuyOrderExecutor has completed");
		}
	}
}