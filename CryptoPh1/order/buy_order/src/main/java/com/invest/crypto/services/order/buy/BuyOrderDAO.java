package com.invest.crypto.services.order.buy;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.invest.crypto.services.order.OrderDAO;
import com.invest.crypto.services.order.OrderManager.OrderState;

/**
 * @author Kiril.m
 */
class BuyOrderDAO extends OrderDAO {

	static List<BuyOrder> getOrders(Connection con/*, int stateId*/) throws SQLException {
		String sql = "select id, market, original_amount, fee, clearance_fee, rate as currency_rate, "
						+ "(original_amount - fee - nvl(clearance_fee, 0)) * rate as base_amount_without_fees "
					+ "from crypto_trade where status = " + OrderState.READY_FOR_PROCESSING.getId();
		List<BuyOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
//			ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BuyOrder order = new BuyOrder(	rs.getLong("id"), rs.getString("market"), rs.getBigDecimal("original_amount"),
												rs.getBigDecimal("base_amount_without_fees"), rs.getBigDecimal("fee"),
												rs.getBigDecimal("clearance_fee"), rs.getBigDecimal("currency_rate"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean lockOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_trade set status = " + OrderState.LOCKED.getId()
					+ " where status = " + OrderState.READY_FOR_PROCESSING.getId() + " and id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean unlockOrder(Connection con, long orderId, String orderUuid, BigDecimal quantity, BigDecimal rate, OrderState state, String comments, BigDecimal commission) throws SQLException {
		String sql = "update crypto_trade set status = ?, uuid = ?, time_opened = sysdate, quantity = ?, bittrex_commission = ?, rate_market = ?, comments = ? "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, state.getId());
			if (orderUuid == null) {
				ps.setNull(2, Types.VARCHAR);
			} else {
				ps.setString(2, orderUuid);
			}
			ps.setBigDecimal(3, quantity);
			ps.setBigDecimal(4, commission);
			ps.setBigDecimal(5, rate);
			if (comments == null) {
				ps.setNull(6, Types.VARCHAR);
			} else {
				ps.setString(6, comments);
			}
			ps.setLong(7, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean updateFailedOrder(Connection con, long orderId, BigDecimal quantity, BigDecimal rate) throws SQLException {
		String sql = "update crypto_trade set status = " + OrderState.PROCESSING_FAILED.getId() + ", quantity = ?, rate_market = ? "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setBigDecimal(1, quantity);
			ps.setBigDecimal(2, rate);
			ps.setLong(3, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean isBuyLimitSkipped(Connection con) throws SQLException {
		String sql = "select config_value from crypto_config where config_key = '" + BuyOrderManager.SKIP_BUY_LIMIT + "'";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getBoolean("config_value");
			}
		}
		return false;
	}

	static Map<String, BigDecimal> getBittrexCommissions(Connection con) throws SQLException {
		Map<String, BigDecimal> result = new HashMap<>();
		String sql = "SELECT crypto_currency, commission FROM bittrex_commissions";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result.put(rs.getString("crypto_currency"), rs.getBigDecimal("commission"));
			}
		}
		return result;
	}
}