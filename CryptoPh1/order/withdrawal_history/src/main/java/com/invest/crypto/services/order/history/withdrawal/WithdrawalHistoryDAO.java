package com.invest.crypto.services.order.history.withdrawal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import com.invest.crypto.services.order.OrderConfig;
import com.invest.crypto.services.order.OrderDAO;
import com.invest.crypto.services.order.history.withdrawal.WithdrawalHistoryManager.WithdrawalHistoryConfigKey;
import com.invest.crypto.services.order.history.withdrawal.WithdrawalHistoryManager.WithdrawalHistoryCurrency;

/**
 * @author Kiril.m
 */
class WithdrawalHistoryDAO extends OrderDAO {

	static OrderConfig getOrderConfig(Connection con) throws SQLException {
		String sql = "select config_key, config_value from crypto_config where config_key in(?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, WithdrawalHistoryConfigKey.API_KEY.toString());
			ps.setString(2, WithdrawalHistoryConfigKey.WITHDRAWAL_HISTORY_SCHEDULED_PERIOD.toString());
			ResultSet rs = ps.executeQuery();
			String apiKey = null;
			long scheduledPeriod = 0;
			while (rs.next()) {
				WithdrawalHistoryConfigKey configKey = WithdrawalHistoryConfigKey.valueOf(rs.getString("config_key"));
				switch (configKey) {
				case API_KEY:
					apiKey = rs.getString("config_value");
					break;

				case WITHDRAWAL_HISTORY_SCHEDULED_PERIOD:
					scheduledPeriod = rs.getLong("config_value");
					break;

				default:
					break;
				}
			}
			if (apiKey != null && scheduledPeriod > 0) {
				return new OrderConfig(apiKey, scheduledPeriod);
			}
			return null;
		}
	}
	
	static Set<WithdrawalHistory> getWithdrawalHistoryFromDb(Connection con, WithdrawalHistoryCurrency currency) throws SQLException {
		String sql = "select payment_uuid, currency, amount, address, opened, authorized, pending_payment, tx_cost, tx_id, canceled, "
					+ "invalid_address from crypto_withdrawal_history where currency = ?";
		Set<WithdrawalHistory> result = new HashSet<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, currency.toString());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result.add(new WithdrawalHistory(	rs.getString("payment_uuid"), rs.getString("currency"), rs.getBigDecimal("amount"),
													rs.getString("address"),
													LocalDateTime.ofInstant(Instant.ofEpochMilli(rs.getTimestamp("opened").getTime()),
																			TimeZone.getDefault().toZoneId()),
													rs.getBoolean("authorized"), rs.getBoolean("pending_payment"), rs.getDouble("tx_cost"),
													rs.getString("tx_id"), rs.getBoolean("canceled"), rs.getBoolean("invalid_address")));
			}
		}
		return result;
	}

	static boolean insertNewWithdrawalHistory(Connection con, Set<WithdrawalHistory> newWithdrawalHistory) throws SQLException {
		String sql = "insert into crypto_withdrawal_history (id, payment_uuid, currency, amount, address, opened, authorized, pending_payment, "
						+ "tx_cost, tx_id, canceled, invalid_address) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		int result = 0;
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			int index = 1;
			for (WithdrawalHistory wh : newWithdrawalHistory) {
				long id = getSequenceNextVal(con, "SEQ_CRYPTO_WITHDRAWAL_HISTORY");
				ps.setLong(index++, id);
				ps.setString(index++, wh.getPaymentUuid());
				ps.setString(index++, wh.getCurrency());
				ps.setBigDecimal(index++, wh.getAmountBd());
				ps.setString(index++, wh.getAddress()); // 5
				ps.setTimestamp(index++, Timestamp.valueOf(wh.getOpened()));
				ps.setBoolean(index++, wh.getAuthorized());
				ps.setBoolean(index++, wh.getPendingPayment());
				ps.setDouble(index++, wh.getTxCost());
				ps.setString(index++, wh.getTxId()); // 10
				ps.setBoolean(index++, wh.getCanceled());
				ps.setBoolean(index++, wh.getInvalidAddress());
				ps.executeUpdate();
				result++;
				index = 1;
			}
		}
		return result == newWithdrawalHistory.size();
	}
}