package com.invest.crypto.services.order.history.withdrawal;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.accountapi.WithdrawalHistoryEntry;
import com.invest.crypto.services.order.OrderConfig;
import com.invest.crypto.services.order.OrderManager;

/**
 * @author Kiril.m
 */
class WithdrawalHistoryManager extends OrderManager {

	private static final Logger log = LogManager.getLogger(WithdrawalHistoryManager.class);

	enum WithdrawalHistoryConfigKey {
		API_KEY, WITHDRAWAL_HISTORY_SCHEDULED_PERIOD;
	}

	enum WithdrawalHistoryCurrency {
		BTC, ETH;
	}

	public static OrderConfig getOrderConfig() {
		Connection con = null;
		try {
			con = getConnection();
			return WithdrawalHistoryDAO.getOrderConfig(con);
		} catch (SQLException e) {
			log.debug("Unable to load config, returning null", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	static Set<WithdrawalHistory> getWithdrawalHistoryFromDb(WithdrawalHistoryCurrency currency) {
		Connection con = null;
		try {
			con = getConnection();
			return WithdrawalHistoryDAO.getWithdrawalHistoryFromDb(con, currency);
		} catch (SQLException e) {
			log.debug("Unable to load withdrawal history from db, returning empty collection", e);
			return new HashSet<>();
		} finally {
			closeConnection(con);
		}
	}

	static Set<WithdrawalHistory> getWithdrawalHistoryFromApi(String apiKey, WithdrawalHistoryCurrency currency) {
		try {
			// TODO apiKey should be used
			List<WithdrawalHistoryEntry> entries = getAccountApi().getWithdrawalHistory(/* apiKey, */currency.toString());
			return convert(entries);
		} catch (SQLException | InvalidKeyException | NoSuchAlgorithmException | IllegalStateException | IOException e) {
			log.debug("Unable to load withdrawal history from api, returning null", e);
			return null;
		}
	}

	private static Set<WithdrawalHistory> convert(List<WithdrawalHistoryEntry> entries) {
		Set<WithdrawalHistory> history = new HashSet<>();
		for (WithdrawalHistoryEntry entry : entries) {
			if (entry.getPaymentUuid() != null && !entry.getPaymentUuid().trim().isEmpty()) {
				history.add(new WithdrawalHistory(entry));
			}
		}
		return history;
	}

	static void insertNewWithdrawalHistory(Set<WithdrawalHistory> newWithdrawalHistory) {
		Connection con = null;
		try {
			con = getConnection();
			if (!WithdrawalHistoryDAO.insertNewWithdrawalHistory(con, newWithdrawalHistory)) {
				log.warn("Insert new withdrawal history in the db did not execute as expected");
			}
		} catch (SQLException e) {
			log.debug("Unable to insert new withdrawal history", e);
		} finally {
			closeConnection(con);
		}
	}
}