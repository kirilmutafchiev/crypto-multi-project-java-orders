package com.invest.crypto.services.order.withdraw;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.invest.crypto.services.order.OrderDAO;
import com.invest.crypto.services.order.OrderManager.OrderState;
import com.invest.crypto.services.order.withdraw.WithdrawOrderManager.EmailParams;

/**
 * @author Kiril.m
 */
class WithdrawOrderDAO extends OrderDAO {

	static List<WithdrawOrder> getOrders(Connection con/* , int stateId */) throws SQLException {
		String sql = "select id, currency, quantity + nvl(bittrex_commission,0) as quantity, wallet, transaction_id from crypto_withdraw where status = " + OrderState.READY_FOR_PROCESSING.getId();
		List<WithdrawOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			// ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				WithdrawOrder order = new WithdrawOrder(rs.getLong("id"), rs.getString("currency"), rs.getBigDecimal("quantity"),
														rs.getString("wallet"), rs.getLong("transaction_id"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean lockOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_withdraw set status = " + OrderState.LOCKED.getId()
					+ " where status = " + OrderState.READY_FOR_PROCESSING.getId() + " and id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean unlockOrder(Connection con, long orderId, String orderUuid) throws SQLException {
		String sql = "update crypto_withdraw set status = " + OrderState.PROCESSED.getId() + ", uuid = ?, time_updated = sysdate "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, orderUuid);
			ps.setLong(2, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean updateFailedOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_withdraw set status = " + OrderState.PROCESSING_FAILED.getId() + ", time_updated = sysdate "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean scheduleTemplateEmail(	Connection con, long userId, long templateId, String params, String from, String to,
											String subject) throws SQLException {
		String sql = "insert into email_templates (id, user_id, type_id, params, time_created, email_from, email_to, subject) "
						+ "values (?, ?, ?, ?, sysdate, ?, ?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			long id = getSequenceNextVal(con, "SEQ_EMAIL_TEMPLATES");
			ps.setLong(1, id);
			ps.setLong(2, userId);
			ps.setLong(3, templateId);
			ps.setString(4, params);
			ps.setString(5, from);
			ps.setString(6, to);
			ps.setString(7, subject);
			return ps.executeUpdate() == 1;
		}
	}
	
	static EmailParams loadEmailParams(Connection con, long transactionId, WithdrawOrderManager manager) throws SQLException {
		String sql = "select first_name, email, id from users where id = (select user_id from transactions where id = ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, transactionId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return manager.new EmailParams(rs.getString("first_name"), rs.getString("email"), rs.getLong("id"));
			}
		}
		return null;
	}
}