package com.invest.crypto.services.order.withdraw;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.accountapi.WithdrawalRequested;

/**
 * @author Kiril.m
 */
class WithdrawOrderTask implements Runnable {

	private static final Logger log = LogManager.getLogger(WithdrawOrderTask.class);
	private String apiKey;
	private WithdrawOrder withdrawOrder;

	public WithdrawOrderTask(String apiKey, WithdrawOrder withdrawOrder) {
		this.apiKey = apiKey;
		this.withdrawOrder = withdrawOrder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		log.debug("Starting withdraw order task for " + withdrawOrder);
		if (WithdrawOrderManager.lockOrder(withdrawOrder.getId())) {
			WithdrawalRequested withdrawRequest = WithdrawOrderManager.placeWithdraw(	apiKey, withdrawOrder.getCurrency(),
																						withdrawOrder.getQuantity(),
																						withdrawOrder.getWallet());
			if (withdrawRequest != null && withdrawRequest.getUuid() != null) {
				WithdrawOrderManager.unlockOrder(withdrawOrder.getId(), withdrawRequest.getUuid());
				WithdrawOrderManager.scheduleTemplateEmail(	withdrawOrder.getTransactionId(), withdrawOrder.getCurrency(),
															withdrawOrder.getQuantity(), withdrawOrder.getWallet());
			} else {
				WithdrawOrderManager.updateFailedOrder(withdrawOrder.getId());
			}
		} else {
			log.debug("The order could not be locked. Probably it's already locked");
		}
		log.debug("Task completed for withdraw order with id " + withdrawOrder.getId());
	}
}